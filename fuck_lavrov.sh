#!/bin/bash

dns_server=$1
number_of_times=$3
server=$2

i=; while [ $(( ( i += 1 ) <= number_of_times )) -ne 0 ]; do
    nslookup $server $dns_server
done
